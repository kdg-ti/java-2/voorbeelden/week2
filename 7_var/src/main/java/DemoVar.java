import be.kdg.model.Student;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * Demo over het gebruik van het nieuwe keyword var
 * var is nieuw sinds JDK10 en kan gebruikt worden
 * bij de declaratie van variabelen.
 */
public class DemoVar {
    public static void main(String[] args) {
        var foo = "Foo";
        System.out.println("foo = " + foo);

        var myList = new ArrayList<Student>();
        myList.add(new Student(123456789, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));
        myList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        myList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        myList.add(new Student(12345, "Sam Gooris", LocalDate.of(1973, 4, 10)));
        myList.add(new Student(12345, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));
        myList.forEach(System.out::println);

        var ints = new int[]{0, 1, 2};
        System.out.println("ints[0] = " + ints[0]);

        var javaScoreMap = new TreeMap<Student, Double>();
        Random random = new Random();
        for (Student student : myList) {
            javaScoreMap.put(student, random.nextDouble() * 20);
        }
        System.out.println("\nTreeMap:");
        for (var entry : javaScoreMap.entrySet()) {
            System.out.printf("student %s behaalde %.1f/20 voor java\n", entry.getKey().getNaam(), entry.getValue());
        }
    }

//    private static var doeIets() {
//        return "done";
//    }
//
//    private void doeIets(var myVar) {
//        System.out.println(myVar);
//    }
}
