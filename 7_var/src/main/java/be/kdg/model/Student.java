package be.kdg.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Basisklasse
 */
public class Student implements Comparable<Student>{
    private final int studNr;  //uniek
    private final String naam;
    private final LocalDate geboorte;
    private String woonplaats;

    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }

    public Student(int studNr, String naam, LocalDate geboorte) {
        this(studNr, naam, geboorte, "");
    }

    public int getStudNr() {
        return studNr;
    }

    public String getNaam() {
        return naam;
    }

    public LocalDate getGeboorte() {
        return geboorte;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }

    @Override
    public int compareTo(Student other) {
        return this.studNr - other.studNr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return studNr == student.studNr;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studNr);
    }
}