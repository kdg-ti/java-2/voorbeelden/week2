import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: derijkej
 */
public class ExerciseVarify {
	public static void main(String[] args) {
		List<Integer> diamond = new ArrayList<>();
		// Hoe schrijf je dit met var?
		var varia = new ArrayList<Integer>();

		diamond = new LinkedList<Integer>();
		// error: incompatible types: LinkedList<Integer> cannot be converted to ArrayList<Integer>
		// varia = new LinkedList<Integer>();
	}
}
